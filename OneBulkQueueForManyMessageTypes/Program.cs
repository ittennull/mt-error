﻿using MassTransit;
using Microsoft.Extensions.DependencyInjection;

var services = new ServiceCollection();
services.AddScoped<MyConsumer>();
        
services.AddMassTransit(x =>
{
    //x.AddScoped<MyConsumerDefinition>();
    //x.AddConsumer<MyConsumer>();
    //x.AddConsumersFromNamespaceContaining<MyConsumer>();
    //x.AddConsumers(typeof(MyMessage).Assembly);
            
    x.UsingRabbitMq((context, cfg) =>
    {
        cfg.ReceiveEndpoint("bbb", e =>
        {
            e.PrefetchCount = 200;
        
            e.Batch<MyMessage>(b =>
            {
                b.MessageLimit = 100;
                b.TimeLimit = TimeSpan.FromSeconds(1);
        
                b.Consumer<MyConsumer, MyMessage>(context);
            });
        });
        
        cfg.ConfigureEndpoints(context);
    });
});

var sp = services.BuildServiceProvider();
var busControl = sp.GetRequiredService<IBusControl>();
await busControl.StartAsync();
Console.ReadKey();
await busControl.StopAsync();


public record MyMessage(string Id);

public class MyConsumer : IConsumer<Batch<MyMessage>>
{
    public Task Consume(ConsumeContext<Batch<MyMessage>> context)
    {
        return Task.CompletedTask;
    }
}

public class MyConsumerDefinition : ConsumerDefinition<MyConsumer>
{
    protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<MyConsumer> consumerConfigurator)
    {
        endpointConfigurator.PrefetchCount = 400;
        
        consumerConfigurator.Options<BatchOptions>(options => options
            .SetMessageLimit(200)
            .SetTimeLimit(s: 10)
            .SetConcurrencyLimit(1));
    }
}
